from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug import generate_password_hash, check_password_hash
from datetime import datetime,tzinfo,timedelta
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
import base64, os

from flask import Flask
app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://focusops:focustest@localhost/local_focusops'
db = SQLAlchemy()

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

db.app = app
db.init_app(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    admin = db.Column(db.Boolean(), unique=False)
    email = db.Column(db.String(120))
    first = db.Column(db.String(120))
    last = db.Column(db.String(120))
    allocation = db.Column(db.Integer)
    deployed = db.Column(db.Integer)
    password = db.Column(db.String(256))
    active = db.Column(db.Boolean())
    vms = db.relationship('Vm', backref='user', lazy='dynamic')

    def __init__(self, email, password, first="", last="", allocation=0, admin=False, active=True):
        self.email = email
        self.set_password(password)
        self.first = first
        self.last = last
        self.admin = admin
        self.allocation = allocation
        self.active = active

    # Some basic shit
    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    #Flask-login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __unicode__(self):
        return self.email

    def __repr__(self):
        return '<User %r>' % (self.email)


class Vm(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.Integer, db.ForeignKey('user.id'))
    veid = db.Column(db.Integer, unique=True)
    hostname = db.Column(db.String(64), unique=True)
    ip = db.Column(db.String(20))
    focusver = db.Column(db.String(64))
    state = db.Column(db.Boolean())
    description = db.Column(db.String(256))

    def __init__(self, owner, veid, hostname, ip, focusver, description):
        self.owner = owner
        self.veid = veid
        self.hostname = hostname
        self.ip = ip
        self.focusver = focusver
        self.description = description
        self.state = False

    def is_running(self):
        return self.state

if __name__ == '__main__':
    manager.run()




