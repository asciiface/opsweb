from flask_wtf import Form
from wtforms import TextField, SubmitField, TextAreaField, SubmitField, SelectField, PasswordField, BooleanField, validators, ValidationError, IntegerField, RadioField
from werkzeug import generate_password_hash, check_password_hash
from wtforms import HiddenField
from models import db, User


class LoginForm(Form):
	email = TextField('Email', [validators.Required("Please enter your email"), validators.Email("Please enter a proper email address")])
	password = PasswordField("Password", [validators.Required("Please enter your password")])
	submit = SubmitField("Login")

	def __init__(self, csfr_enabled=False, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)
		self.user = None
		self.userid = None

	def validate(self):
		rv = Form.validate(self)
		print Form.validate(self)
		print self.errors
		if not rv:
			return False

		user = self.get_user()
		if user is None:
			print "invalid user.."
			 #validators.ValidationError('Invalid User')
			self.email.errors.append("Uknown username or password")
			return False
		if not User.check_password(user, self.password.data):
			print "invalid pass.."
			self.email.errors.append("Uknown username or password")
			return False
			#return validators.ValidationError('Invalid Password')

		self.user = self.email.data
		self.userid = user.id
		return True


	def get_user(self):
		return db.session.query(User).filter_by(email=self.email.data).first()

class PasswordReset(Form):
	oldpassword = PasswordField("Old Password", [validators.Required("Please enter your old password")])
	newpassword = PasswordField("New Password", [validators.Required("Please enter your new password")])
	confirm = PasswordField("Confirm", [validators.Required("Please re-enter your new password"), validators.EqualTo('newpassword', 'Passwords must match.')])
	submit = SubmitField("Update")

	def __init__(self, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)

	def reset_password(user, password):
		usermod = db.session.query(User).filter_by(email=user.email.data).first()
		usermod.password = User.generate_password_hash(usermod.password, password)

class AddUser(Form):
	admin = BooleanField('Administrator', default=False)
	email = TextField('Email', [validators.Required("Please enter the email"), validators.Email("Please enter a proper email address"), validators.Length(min=5, max=50)])
	first = TextField('First Name', [validators.Required("Please enter users First name")])
	last = TextField('Last Name', [validators.Required("Please enter users Last name")])
	allocation = IntegerField('VM Allocation', [validators.Required("Please enter users VM allocation")])
	password = PasswordField("Password", [validators.Required("Please enter the new users password."), validators.Length(min=6, max=256)])
	confirm = PasswordField("Password Confirm", [validators.Required("Please confirm the new users password."), validators.EqualTo('password', 'Passwords must match.'), validators.Length(min=6, max=256)])
	submit = SubmitField("Add!")

	def __init__(self, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)

class AddVM(Form):
    owner = SelectField(U'Owner', coerce=int)
    ipaddr = TextField('Ip Address')
    veid = TextField('VEID')
    focusver = SelectField(u'Focus Version', [validators.Required("Please select a focus version")], choices=[('5.3.8', '5.3.8'), ('5.3.9', '5.3.9'), ('6.0', '6.0')])
    description = TextField('Description', [validators.Length(min=0, max=256)])
    submit = SubmitField("Deploy")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)



class ModifyUser(Form):
	admin = BooleanField('Administrator', default=False)
	#admin = RadioField('Administrator', choices=[['value','description']])
	email = TextField('Email', [validators.Required("Please enter the email"), validators.Email("Please enter a proper email address")])
	first = TextField('First Name')
	last = TextField('Last Name')
	allocation = IntegerField('VM Allocation')
	deployed = IntegerField('Deployed VMs')
	password = PasswordField("Password")
	submit = SubmitField("Update")


	def __init__(self, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)

