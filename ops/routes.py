from ops import app
from flask import Flask, render_template, request, flash, redirect, url_for, session, g
from flask import send_from_directory
from flask.ext.login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, AnonymousUserMixin,
                            confirm_login, fresh_login_required)
from forms import LoginForm, ModifyUser, PasswordReset, AddUser, AddVM
from models import db, User, Vm
import os, random, string

appName = "Focus Ops"
#app.permanant_session_lifetime = timedelta(seconds=)

IPS = ['127.0.0.1', '1.1.1.2', '1.1.1.3', '1.1.1.4', '1.1.1.5', '1.1.1.6', '1.1.1.7', '1.1.1.8']
VEIDS = [5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009, 5010]

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

class Anonymous(AnonymousUserMixin):
	name = u"Anon"

login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id):
	return db.session.query(User).get(user_id)

@app.before_request
def  before_request():
    g.user = current_user
    if current_user.is_authenticated():
        userid = User.query.filter_by(id=current_user.get_id()).first()
        vms = User.query.filter_by(id=current_user.get_id()).first().vms.order_by(Vm.id).all()
        if len(vms) != userid.deployed:
            userid.deployed = len(vms)
            db.session.commit()

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.route("/favicon.ico")
def favicon():
	return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/")
def home():
	return render_template("home.html")

@app.route("/profile")
@login_required
def profile():
	#flash("This will be your user profile! Nothing too special.")
    vms = User.query.filter_by(id=current_user.get_id()).first().vms.order_by(Vm.id).all()
    userid = User.query.filter_by(id=current_user.get_id()).first()


    return render_template('profile.html', userid=userid, vms=vms)

@app.route("/vms")
@login_required
def vms():
    vmuser = User.query.filter_by(id=current_user.get_id()).first()
    vms = user = User.query.filter_by(id=current_user.get_id()).first().vms.order_by(Vm.id).all()
    flash("Here will be a list of your VMs and some options to... do things")
    return render_template('vms.html', user=vmuser, vms=vms)

@app.route("/allvms")
@login_required
def allvms():
    vmuser = User.query.filter_by(id=current_user.get_id()).first()
    vms = user = Vm.query.all()
    flash("Here will be a list of *ALL* VMs and some options to... do things")
    return render_template('vms.html', user=vmuser, vms=vms, userobj=User)

@app.route("/login", methods=['POST', 'GET'])
def login():
	form = LoginForm(request.form)
	#print form.email.data
	#print form.password.data

	if form.validate_on_submit():
		#session['email'] = form.email.data
		print "%s trying to log in" % (User.query.filter_by(email=form.email.data).first())
		if login_user(User.query.filter_by(email=form.email.data).first()):
			flash("You are logged in.")
		else:
			flash("Login failed")

		return redirect(url_for('home'))

	return render_template('login.html', form=form)

@app.route("/test")
def test():
	login_user(User.query.filter_by(email="test@test.com").first())
	return redirect(url_for('home'))

@app.route("/logout")
def logout():
	#if 'email' not in session:
	#	flash("You aren't logged in...")
	#	return redirect(url_for('home'))

	#session.clear()
	logout_user()
	flash("You have been logged out.")
	return redirect(url_for('home'))

@app.route("/resetpwd", methods=['POST', 'GET'])
@login_required
def resetpwd():
	form = PasswordReset()
	
	if request.method == 'GET':
		return render_template("pwdreset.html", form=form, userid=User.query.filter_by(id=current_user.get_id()).first())
	else:

		if form.validate_on_submit():
			user = User.query.filter_by(id=current_user.get_id()).first()
			print "%s is trying to reset their password" % user.email
			if User.check_password(user, form.oldpassword.data):
				print "old: %s" % form.oldpassword.data
				print "new: %s" % form.newpassword.data
				print "confirm: %s" % form.confirm.data
				userupdate = User.query.filter_by(id=user.id).first()
				User.set_password(userupdate, form.newpassword.data)
				flash("Password updated!")
				db.session.commit()
				return redirect(url_for('profile'))
			else:
				form.oldpassword.errors.append("Old password does not match!")

			return render_template("pwdreset.html", form=form, userid=User.query.filter_by(id=current_user.get_id()).first())

		return render_template("pwdreset.html", form=form, userid=User.query.filter_by(id=current_user.get_id()).first())

@app.route("/listusers")
@login_required
def listusers():
	if not User.query.filter_by(id=current_user.get_id()).first().admin:
		flash("Admins only!")
		return redirect(url_for('home'))
	admins = User.query.filter_by(admin=True).all()
	allusers = User.query.filter_by(admin=False).all()
	return render_template('admin.html', allusers=allusers, admins=admins)

@app.route("/adduser", methods=['POST', 'GET'])
@login_required
def adduser():
	if not User.query.filter_by(id=current_user.get_id()).first().admin:
		flash("Admins only!")
		return redirect(url_for('home'))
	form = AddUser()
	if request.method == 'GET':
		return render_template("adduser.html", form=form)	

	if request.method == 'POST':
		if form.validate_on_submit():
			useradd = User(form.email.data, form.password.data, form.first.data, form.last.data, form.allocation.data, form.admin.data)
			db.session.add(useradd)
			db.session.commit()
			return redirect(url_for('listusers'))
		else:
			return render_template("adduser.html", form=form)


@app.route("/toggleactive/<userid>", methods=['POST', 'GET'])
@login_required
def toggleactive(userid):
    if not User.query.filter_by(id=current_user.get_id()).first().admin:
        flash("Admins only!")
        return redirect(url_for('home'))

    if request.method == 'GET':
        return redirect(url_for('listusers'))

    elif request.method == 'POST':
        user = User.query.filter_by(id=userid).first()
        # If they are active
        if user.active:
            if user.admin:
                flash("%s is an ADMIN! You must demote the user before disabling!" % user.email)
                return redirect(url_for('listusers'))
            elif user.id == User.query.filter_by(id=current_user.get_id()).first().id:
                flash("This is you, you cannot disable yourself. Silly buns!" % user.email)
                return redirect(url_for('listusers'))
        
        user.active = not user.active
        if user.active:
            flash("%s has been enabled" % user.email)
        else:
            flash("%s has been disabled" % user.email)
        db.session.commit()
        return redirect(url_for('listusers'))

@app.route("/terminateuser/<userid>", methods=['POST', 'GET'])
@login_required
def terminateuser(userid):
	if not User.query.filter_by(id=current_user.get_id()).first().admin:
		flash("Admins only!")
		return redirect(url_for('home'))

	if request.method == 'GET':
		return redirect(url_for('listusers'))

	if request.method == 'POST':
		if User.query.filter_by(id=userid).first().admin:
			flash("This user is an ADMIN! You must demote the user before deleting!")
			return redirect(url_for('listusers'))
		flash("User %s has been deleted!" % User.query.filter_by(id=userid).first().email)
		user = User.query.filter_by(id=userid).first()
        if user.vms.all():
            for i in user.vms.all():
                db.session.delete(i)
        db.session.delete(user)
        db.session.commit()
        return redirect(url_for('listusers'))		

@app.route("/modvm/<userid>/<command>/<veid>", methods=['POST', 'GET'])
@login_required
def modvm(userid):
    return True

@app.route("/createvm", methods=['POST', 'GET'])
@login_required
def createvm():
    vms = User.query.filter_by(id=current_user.get_id()).first().vms.order_by(Vm.id).all()
    allvms = Vm.query.all()
    userid = User.query.filter_by(id=current_user.get_id()).first()
    allusers = User.query.all()
    form = AddVM()
    availableveid = []
    availableip = []
    usedveid = []
    usedip = []

    for i in allvms:
        usedveid.append(int(i.veid))
        usedip.append(i.ip)
        
    for i in VEIDS:
        if i not in usedveid:
            availableveid.append(int(i))

    for i in IPS:
        if i not in usedip:
            availableip.append(i)

    if not User.query.filter_by(id=current_user.get_id()).first().admin:            
        form.owner.choices = [(int(userid.id), userid.email)]
        print form.owner.choices
    else:
        form.owner.choices = [(int(g.id), g.email) for g in allusers]
        print form.owner.choices


    userveid = random.choice(availableveid)
    userip = random.choice(availableip)

    print "Available VEIDS: %s" % availableveid
    print "Available IPS: %s" % availableip

    if request.method == 'GET':
        return render_template("deployvm.html", userid=userid, vms=vms, form=form, userveid=userveid, userip=userip)

    elif request.method == 'POST':
        if form.validate_on_submit():
            deployuserid = User.query.filter_by(id=form.owner.data).first()
            vms = User.query.filter_by(id=form.owner.data).first().vms.order_by(Vm.id).all()
            vmsafter = len(vms) + 1
            if vmsafter > deployuserid.allocation:
                flash("You are at your limit! Cannot deploy more VMs.")
                return redirect(url_for("vms"))
            else:
                hostname = "development%s.%s" % (id_generator(),deployuserid.last)
                print form.description.data
                newvm = Vm(form.owner.data, form.veid.data, hostname, form.ipaddr.data, form.focusver.data, form.description.data)
                try:
                    db.session.add(newvm)
                    db.session.commit()
                    flash("%s has been created!" % (newvm.hostname))
                except:
                    flash("Something went wrong deploying your VM")
                return redirect(url_for("vms"))
                
        return render_template("deployvm.html", userid=userid, vms=vms, form=form, userveid=userveid, userip=userip)

    else:
        return redirect(url_for('vms'))


@app.route("/termvm/<userid>/<veid>")
@login_required
def termvm(userid, veid):
    try:
        vms = User.query.filter_by(id=userid).first().vms.first()
        user = User.query.filter_by(id=userid).first()
        loggeduser = User.query.filter_by(id=current_user.get_id()).first()
    except:
        flash("Invalid ID or error")
        return redirect(url_for("vms"))

    if not loggeduser.admin:
        if not int(user.id) == int(userid):
            print user.id
            print userid
            flash("You don't own this VM!")
            return redirect(url_for("vms"))

    print "Deleting %s confirmed by %s" % (veid, loggeduser.email)

    if vms.is_running():
        flash("VM must be shut down first.")
        return redirect(url_for("vms"))

    flash("The VM [%s] has been deleted!" % vms.hostname)
    db.session.delete(vms)
    db.session.commit()

    return redirect(url_for("vms"))


@app.route("/modifyuser/<userid>", methods=['POST', 'GET'])
@login_required
def moduser(userid):
    if not User.query.filter_by(id=current_user.get_id()).first().admin:
        flash("Admins only!")
        return redirect(url_for('home'))
    
    form = ModifyUser()
    modusername = User.query.filter_by(id=userid).first()

    if request.method == 'POST':
        print "starting moduser POST"
        if form.validate_on_submit():
            userupdate = User.query.filter_by(id=userid).first()
            userupdate.admin = form.admin.data
            userupdate.email = form.email.data
            userupdate.first = form.first.data
            userupdate.last = form.last.data
            userupdate.allocation = form.allocation.data
            userupdate.deployed = form.deployed.data
            if not form.password.data:
                print "password not reset"
            else:
                User.set_password(userupdate, form.password.data)
                flash("Password updated!")
                db.session.commit()
                print "LIST USER"
                #return redirect(url_for('listusers'))
            flash("User modified...")
            db.session.commit()
            return redirect(url_for('listusers'))
        else:
            print "FAILED VALIDATE"
            #return redirect(url_for('moduser.html', form=form, userid=userid, modusername=modusername))
            #return render_template(url_for('moduser.html', form=form, userid=userid, modusername=modusername))
            return render_template("moduser.html", form=form, userid=userid, modusername=modusername)

    elif request.method == 'GET':
        return render_template("moduser.html", form=form, userid=userid, modusername=modusername)


@app.route("/admin", methods=['POST', 'GET'])
@login_required
def admin():
	if not User.query.filter_by(id=current_user.get_id()).first().admin:
		flash("Admins only!")
		return redirect(url_for('home'))
	form = ModifyUser(request.form)
	if request.method == 'POST':
		return redirect(url_for('listusers'))

	form = ModifyUser()

	
	flash("I just wanted to flash something...")
	return render_template("admin.html", form=form)





if __name__ == "__main__":
	app.run(debug=True)