#!/usr/bin/python
#/home/focusops/public_html/focusops/ops
import sys, os
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/home/focusops/public_html/focusops/")
#sys.path.insert(0, "/home/asciifaceman/Web/voran/")
#activate_this = '/home/asciifaceman/Web/voran/voran/venv/bin/activate'
activate_this = '/home/focusops/public_html/focusops/ops/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))
 
from ops import app as application
application.secret_key = 'thissecretkey'